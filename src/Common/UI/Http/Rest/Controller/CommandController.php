<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018-10-09
 * Time: 14:17
 */

namespace App\Common\UI\Http\Rest\Controller;

use League\Tactician\CommandBus;


class CommandController
{

    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus){

        $this->commandBus = $commandBus;
    }

    protected function exec($command){

    }
}