<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018-10-09
 * Time: 11:36
 */

namespace App\News\UI\Http\Web\Controller;


use App\Common\UI\Http\Web\Controller\AbstractWebController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractWebController
{
    /**
     * @Route(
     *     "/",
     *     name="homepage",
     *     methods={"get"}
     *     )
     */
    public function index(){
        return $this->render('News/Web/home/index.html.twig');
    }
}